/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.duck.shape;

/**
 *
 * @author Administrator
 */
public class Sqaure extends Shape{
    double side;

    public Sqaure(double side){
    super("Sqaure");
    this.side=side;
    }
    @Override
    public double calArea() {
       return side*side; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String toString() {
        return "Saure{" + "side=" + side + '}';
    }

    public void setSide(double side) {
        this.side = side;
    }

    public double getSide() {
        return side;
    }
    
}
