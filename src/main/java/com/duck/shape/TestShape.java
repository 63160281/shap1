/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.duck.shape;

/**
 *
 * @author Administrator
 */
public class TestShape {
    public static void main(String[] args) {
        Circle c1 = new Circle(1.5);
        Circle c2 = new Circle(4.5);
        Circle c3 = new Circle(5.5);
        Rectangle r1 = new Rectangle(3,2);
        Rectangle r2 = new Rectangle(4,3);
        Sqaure s1 = new Sqaure(2);
        Sqaure s2 = new Sqaure(4);
        System.out.println(c1);
        System.out.println(c2);
        System.out.println(c3);
        System.out.println(r1);
        System.out.println(r2);
        System.out.println(s1);
        System.out.println(s2);
        
        Shape[] shapes = {c1 , c2, c3, r1, r2, s1, s2};
        for(int i=0; i<shapes.length; i++){
            System.out.println(shapes[i].getName()+" Area :"+shapes[i].calArea());
        }
    }
}
