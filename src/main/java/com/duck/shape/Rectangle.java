/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.duck.shape;

/**
 *
 * @author Administrator
 */
public class Rectangle extends Shape{
    private double wide;
    private double high;
    public Rectangle(double wide, double high){
      super("Rectangle");
      this.wide = wide;
      this.high = high;
    }
    public double getWide() {
      return wide;
    }
    public void setWide(double wide){
      this.wide = wide;
    }
    public double gethigh() {
      return high;
    }
    public void sethigh(double high){
      this.high = high;
    }
    @Override
    public double calArea(){
      return wide * high;
    }
    @Override
    public String toString(){
      return "Rectangle{"+"wide="+wide+",high="+high+'}';
    }
}